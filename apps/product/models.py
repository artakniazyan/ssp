from __future__ import unicode_literals

from django.db import models

# from apps.category.models import Category


class Product(models.Model):
    name = models.TextField()
    image = models.ImageField(max_length=256)
    # category = models.ForeignKey(Category, related_name='products')

    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name
