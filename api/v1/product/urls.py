from rest_framework.routers import DefaultRouter

from api.v1.product.api import ProductViewSet

router = DefaultRouter()
router.register(r'product', ProductViewSet, base_name='product')

urlpatterns = [

]

urlpatterns += router.urls
