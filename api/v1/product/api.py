from rest_framework import viewsets, mixins

from api.v1.product.serializers import ProductSerializer
from apps.product.models import Product


class ProductViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
